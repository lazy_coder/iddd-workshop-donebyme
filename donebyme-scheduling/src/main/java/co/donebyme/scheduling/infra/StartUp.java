package co.donebyme.scheduling.infra;

import co.donebyme.scheduling.infra.messaging.MatchingContextSubscriber;

public class StartUp {
  public static void main(final String[] args) {
//    PricingJournal.start();
//    PricingJournalPublisher.start();
    MatchingContextSubscriber.start();
  }
}
