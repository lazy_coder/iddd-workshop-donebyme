package co.donebyme.scheduling.infra.messaging;

import co.vaughnvernon.mockroservices.messagebus.Message;
import co.vaughnvernon.mockroservices.messagebus.MessageBus;
import co.vaughnvernon.mockroservices.messagebus.Subscriber;
import co.vaughnvernon.mockroservices.messagebus.Topic;

public class MatchingContextSubscriber implements Subscriber {
  private static MatchingContextSubscriber instance;
  
  private final Topic matchingTopic;
  
  public static MatchingContextSubscriber start() {
    	if (instance == null) {
    		instance = new MatchingContextSubscriber();
    	}
    	return instance;
  }
  
  @Override
  public void handle(final Message message) {
  }
  
  private MatchingContextSubscriber() {
    final MessageBus messageBus = MessageBus.start("donebyme");
    this.matchingTopic = messageBus.openTopic("matching");
    
    matchingTopic.subscribe(this);
  }
}
